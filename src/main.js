/* eslint-disable func-names */
/* eslint-disable import/no-unresolved */
// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import Buefy from 'buefy';
import '~/main.scss';
import Axios from 'axios';
import DefaultLayout from '~/layouts/Default.vue';

export default function (Vue, { head }) {
  // Add attributes to HTML tag
  // eslint-disable-next-line no-param-reassign
  head.htmlAttrs = { lang: 'fr' };

  // import vue meta
  head.meta.push({
    name: 'viewport',
    content: 'width=device-width, initial-scale=1.0, shrink-to-fit=no',
  });

  // Import Font Awesome
  head.link.push({
    rel: 'stylesheet',
    href: 'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
  });

  head.link.push({
    rel: 'preconnect',
    href: 'https://fonts.gstatic.com',
  });

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@400&display=swap',
  });

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700;800&display=swap', // secondary text
  });

  // Set default layout as a global component
  Vue.component('LayoutVue', DefaultLayout);
  // Register our Bulma component library
  Vue.use(Buefy);
  // http requests via axios
  Vue.use(Axios);
}

---
title: Présentation des indicateurs de performances de la collecte de données au Burkina-Faso
published: true
date: 2022-11-15
---

**Liste des indicateurs du Burkina-Faso**  

**1- Nombre de produits**, indique le nombre de produits distribués au cours du programme pendant une période P.

**2- Nombre de centres**, indique le nombre de centres de santé et de pharmacies inclus dans le programme.

**3- Nombre de bénéficiaires**, indique le nombre de personnes prises en charge dans le cadre du programme pendant une période P.

**4- Taux de complétude des rapports**, présente l'évolution mensuelle du taux de complétude des rapports dans les divers centres du programme pendant une période P. Il est défini par le nombre de rapport transmis par rapport au nombre attendu sur la période définie.

**6- Taux de péremption de produits**, présente l'évolution mensuelle du pourcentage de produits périmés dans les centres de santé et pharmacies pendant une période P.

**7- Taux d'avaries de produits**, présente l'évolution mensuelle du pourcentage de produits avariés dans les centres de santé et pharmacie pendant une période P </p>

**8- Taux de centres ayant connu une rupture de stock**, présente l'évolution mensuelle du pourcentage de centres de santés et de pharmacies ayant connu une rupture de stock sur l’un des produits distribués pendant une période P.

**9- Taux de produits ayant un niveau de stock adéquat**>, présente l'évolution mensuelle du pourcentage de produits ayant maintenu un niveau de stock adéquat (entre la quantité minimale et maximale requise) pendant une période P.

**10- Taux de produits ayant connu une rupture de stock**, présente l'évolution mensuelle du pourcentage de produits ayant connu une rupture de stock pendant une période P.

**11- Durée moyenne de rupture de stock**, indique le nombre moyen de jours d'indisponibilité de produits pour chaque mois de la période P visualisée.

**12- Taux de satisfaction d’approvisionnement**, présente l'évolution mensuelle du pourcentage de centres de santés et de pharmacies ayant réceptionnées des produits conforme aux demandes effectuées dans le cadre du programme pendant une période P.

**13- Taux de dispensation**, présente l'évolution mensuelle du pourcentage de personnes prises en charge dans le cadre du programme pendant une période P.

**14- Centres les plus souvent en ruptures**, présente les centres de santés et pharmacies les plus souvent en rupture de stock pendant une période P.  


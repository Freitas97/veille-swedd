# Occop Viz

### Getting started

Run the following command on your local environment:

```
git clone <url> --depth 1
cd veille-swedd
npm install
```

Then, you can run locally in development mode with live reload:

```
yarn develop

```

Open http://localhost:3000 with your favorite browser to see your project.

### Commit Validation

For every push or commit, you need to use the this command:

```
$ git add .
$ yarn commit or npm run commit
? Select the type of change that you're committing: (Use arrow keys)
❯ feat:     A new feature 
  fix:      A bug fix 
  docs:     Documentation only changes 
  style:    Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) 
  refactor: A code change that neither fixes a bug nor adds a feature 
  perf:     A code change that improves performance 
  test:     Adding missing tests or correcting existing tests 
$ git push
```

### Code Formating and optimization validations

Code Formating and optimizations (unused imports, variables and more) are automatically verified on every commit before push. No action is required except to correct the reported problems if the commit fails

```
✔ Preparing lint-staged...
✔ Hiding unstaged changes to partially staged files...
❯ Running tasks for staged files...
  ❯ lint-staged.config.js — 32 files
    ❯ *.{vue} — 12 files
      ⠋ eslint --fix
      ◼ eslint
    ◼ **/*.vue — 7 files
    ◼ *.json — 7 files
◼ Applying modifications from tasks...
```

### Repository Structure
```
.
├── README.md                # README file
├── src
│   ├── layouts              # Layout components
|   ├── assets               # Assets folders
│   ├── pages                # Vue pages
│   ├── templates            # Default template
└── variables.scss           # Global variables
└── jsconfig.json            # JavaScript configuration
```

### Deploy to production

```
$ yarn build

```
